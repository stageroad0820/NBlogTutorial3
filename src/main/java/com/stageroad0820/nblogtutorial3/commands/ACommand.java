package com.stageroad0820.nblogtutorial3.commands;

import com.stageroad0820.nblogtutorial3.NBlogTutorial3;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabExecutor;

import java.util.List;

public abstract class ACommand implements TabExecutor {
	protected NBlogTutorial3 nblog;
	private String label;

	public ACommand(NBlogTutorial3 plugin, String commandLabel) {
		this.nblog = plugin;
		this.label = commandLabel;
	}

	public String getLabel() {
		return label;
	}

	public abstract List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args);
	public abstract boolean onCommand(CommandSender sender, Command command, String label, String[] args);
}
