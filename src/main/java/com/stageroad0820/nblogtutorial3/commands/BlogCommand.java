package com.stageroad0820.nblogtutorial3.commands;

import com.stageroad0820.nblogtutorial3.NBlogTutorial3;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class BlogCommand extends ACommand {
	public BlogCommand(NBlogTutorial3 plugin, String commandLabel) {
		super(plugin, commandLabel);
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
		List<String> tabs = new ArrayList<>();

		if (args.length == 1) {
			tabs.add("help");
			tabs.add("info");
			tabs.add("random");
		}

		return tabs;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (sender instanceof Player) {
			Player player = (Player) sender;

			if (label.equalsIgnoreCase("blog")) {
				if (args.length == 0) {
					player.sendMessage(ChatColor.RED + "명령어의 인자가 너무 적거나 없습니다! " + ChatColor.YELLOW + "/blog help" + ChatColor.RED + " 명령어를 통해 도움말을 확인할 수 있습니다.");
				}
				else {
					if (args[0].equalsIgnoreCase("help")) {
						player.sendMessage(ChatColor.GRAY + "==================================================");
						player.sendMessage(ChatColor.AQUA + nblog.getFullName() + ChatColor.WHITE + " 플러그인에 포함된 명령어입니다: ");
						player.sendMessage(ChatColor.GREEN + "/blog help" + ChatColor.WHITE + " : 이 플러그인의 도움말을 출력합니다.");
						player.sendMessage(ChatColor.GREEN + "/blog info" + ChatColor.WHITE + " : 이 플러그인의 정보를 출력합니다.");
						player.sendMessage(ChatColor.GREEN + "/blog random" + ChatColor.WHITE + " : 1부터 100사이의 수 중 무작위 숫자 하나를 출력합니다.");
						player.sendMessage(ChatColor.GRAY + "==================================================");
					}
					else if (args[0].equalsIgnoreCase("info")) {
						player.sendMessage(ChatColor.GRAY + "==================================================");
						player.sendMessage(ChatColor.GOLD + " - 플러그인 이름 : " + ChatColor.WHITE + nblog.getPdfFile().getName());
						player.sendMessage(ChatColor.GOLD + " - 플러그인 버전 : " + ChatColor.WHITE + nblog.getPdfFile().getVersion());
						player.sendMessage(ChatColor.GOLD + " - 플러그인 개발자(들) : " + ChatColor.WHITE + nblog.getPdfFile().getAuthors());
						player.sendMessage(ChatColor.GOLD + " - 플러그인 메인 클래스 : " + ChatColor.WHITE + nblog.getPdfFile().getMain());
						player.sendMessage(ChatColor.GOLD + " - 설명 : " + ChatColor.WHITE + nblog.getPdfFile().getDescription());
						player.sendMessage(ChatColor.GRAY + "==================================================");
					}
					else if (args[0].equalsIgnoreCase("random")) {
						int random = (int)(Math.random() * 100 + 1);

						player.sendMessage(ChatColor.WHITE + "플러그인이 무작위로 생성한 번호는 " + ChatColor.AQUA + random + ChatColor.WHITE + "입니다!");
					}
				}
			}
		}

		return false;
	}
}
