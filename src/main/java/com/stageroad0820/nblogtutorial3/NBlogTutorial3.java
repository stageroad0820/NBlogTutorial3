package com.stageroad0820.nblogtutorial3;

import com.stageroad0820.nblogtutorial3.commands.BlogCommand;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Objects;

public class NBlogTutorial3 extends JavaPlugin {
	PluginDescriptionFile pdfFile = this.getDescription();

	String pfName = pdfFile.getName() + " v" + pdfFile.getVersion();

	public String getFullName() {
		return pfName;
	}

	public PluginDescriptionFile getPdfFile() {
		return pdfFile;
	}

	@Override
	public void onEnable() {
		BlogCommand cmd_blog = new BlogCommand(this, "blog");

		Objects.requireNonNull(getCommand(cmd_blog.getLabel())).setExecutor(cmd_blog);
		Objects.requireNonNull(getCommand(cmd_blog.getLabel())).setTabCompleter(cmd_blog);

		NBlogTutorial3.console(ChatColor.YELLOW + pfName + ChatColor.WHITE + " 이(가) 활성화되었습니다!");
		super.onEnable();
	}

	@Override
	public void onDisable() {
		NBlogTutorial3.console(ChatColor.YELLOW + pfName + ChatColor.WHITE + " 이(가) 비활성화되었습니다!");
		super.onDisable();
	}

	public static void console(String message) {
		Bukkit.getConsoleSender().sendMessage(message);
	}
}
